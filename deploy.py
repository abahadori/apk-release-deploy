#!/usr/bin/python3
#
# Copyright (C) 2019 Oleg Shnaydman
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import os
import argparse
import requests
import json
import re
import shutil
from os import walk

DROPBOX_ERROR_CODE = 1
GET_TOKEN_ERROR_CODE = 2
TEMPLATE_ERROR_CODE = 3
CHANGES_ERROR_CODE = 4
OUTPUT_FILE_PARSING_ERROR = 5
TELEGRAM_ERROR_CODE = 6

DROPBOX_UPLOAD_ARGS = {
    'path': None,
    'mode': 'overwrite',
    'autorename': True,
    'strict_conflict': True
}
DROPBOX_UPLOAD_URL = 'https://content.dropboxapi.com/2/files/upload'

DROPBOX_SHARE_DATA = {
    'path': None,
    'settings': {
        'requested_visibility': 'public'
    }
}
DROPBOX_SHARE_URL = 'https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings'

DROPBOX_DELETE_DATA = {
    'path': None
}
DROPBOX_DELETE_URL = 'https://api.dropboxapi.com/2/files/delete_v2'


def get_token(refresh_token, client_id, client_secret):
    '''
    Refreshes an access token using a refresh token.

    Args:
        refresh_token (str): The refresh token obtained during the initial OAuth 2.0 authorization.
        client_id (str): The client ID of your Dropbox app.
        client_secret (str): The client secret of your Dropbox app.

    Returns:
        str or None: The new access token if successful, or None on failure.
    '''
    url = "https://api.dropbox.com/oauth2/token"
    payload = {'grant_type': 'refresh_token',
               'refresh_token': refresh_token,
               'client_id': client_id,
               'client_secret': client_secret}
    r = requests.post(url, data=payload)
    if r.status_code != requests.codes.ok:
        print("Failed: get token: {errcode}".format(
            errcode=r.status_code))
        print("get token response: {r}".format(r=r.text))

        if r.status_code == 401:
            print("Attention: current refresh token is expired. Try to refresh it.")
        return None

    tokens = r.json()
    return tokens['access_token']


def upload_to_dropbox(target_file_name, source_file, dropbox_token, dropbox_folder):
    '''Upload file to dropbox

    Args:
        target_file_name (str): Uploaded file will be rename to this file name.
        source_file (str): File that is going to be uploaded.
        dropbox_token (str): Dropbox API key.
        dropbox_folder (str): Dropbox target folder.

    Returns:
        str: Shared url for download.
    '''
    dropbox_path = '/{folder}/{file_name}'.format(
        folder=dropbox_folder, file_name=target_file_name)
    # print(dropbox_path)
    DROPBOX_UPLOAD_ARGS['path'] = dropbox_path
    DROPBOX_SHARE_DATA['path'] = dropbox_path
    DROPBOX_DELETE_DATA['path'] = dropbox_path
    print("dropbox path: {dropbox_path}".format(dropbox_path=dropbox_path))
    # Try to delete the file before upload
    # It's possible to overwrite but this way is cleaner
    headers = {'Authorization': 'Bearer ' +
               dropbox_token, 'Content-Type': 'application/json'}
    requests.post(DROPBOX_DELETE_URL, data=json.dumps(
        DROPBOX_DELETE_DATA), headers=headers)

    headers = {'Authorization': 'Bearer ' + dropbox_token,
               'Dropbox-API-Arg': json.dumps(DROPBOX_UPLOAD_ARGS),
               'Content-Type': 'application/octet-stream'}

    # Upload the file
    r = requests.post(DROPBOX_UPLOAD_URL, data=open(
        source_file, 'rb'), headers=headers)

    if r.status_code != requests.codes.ok:
        print("Failed: upload file to Dropbox: {errcode}".format(
            errcode=r.status_code))
        print("upload response: {r}".format(r=r.text))
        return None

    headers = {'Authorization': 'Bearer ' + dropbox_token,
               'Content-Type': 'application/json'}

    # Share and return downloadable url
    r = requests.post(DROPBOX_SHARE_URL, data=json.dumps(
        DROPBOX_SHARE_DATA), headers=headers)

    if r.status_code != requests.codes.ok:
        print("Failed: get share link from Dropbox {errcode}".format(
            errcode=r.status_code))
        return None

    # Replace the '0' at the end of the url with '1' for direct download
    app_download_url = re.sub('dl=.*', 'raw=1', r.json()['url'])
    print("App download url: {link}".format(link=app_download_url))
    return app_download_url


def send_message_to_telegram(bot_token, chat_id, caption=''):
    '''Send a message to a Telegram chat.

    Args:
        bot_token (str): The Telegram Bot API token used to authenticate with Telegram.
        chat_id (str): The unique identifier of the chat or recipient.
        caption (str, optional): The text message to be sent as a caption (default is an empty string).

    Returns:
        bool: True if the message was sent successfully, False otherwise.
    '''
    try:
        # Construct the URL for sending a message to Telegram using the provided bot_token
        url = f'https://api.telegram.org/bot{bot_token}/sendMessage'

        # Prepare the data to be sent in the POST request
        data = {
            'chat_id': chat_id,
            'text': caption,
        }

        # Send the POST request to the Telegram API
        response = requests.post(url, data=data)

        # Check if the request was successful (HTTP status code 200)
        if response.status_code == 200:
            print('Message sent to telegram successfully')
            return True
        else:
            # If the request was not successful, print an error message and the response text
            print(
                f'Failed to send message. Status code: {response.status_code}')
            print(response.text)

            return False
    except Exception as e:
        # Handle any exceptions that may occur during the execution of the code
        print(f'Error: {e}')
        return False


def send_to_telegram(bot_token, chat_id, app_file, target_app_file, caption=''):
    '''Send a document (file) to a Telegram chat and optionally include a caption.

    Args:
        bot_token (str): The Telegram Bot API token used to authenticate with Telegram.
        chat_id (str): The unique identifier of the chat or recipient.
        app_file (str): The path to the source document (file) to be sent.
        target_app_file (str): The path to the copied document (file) to be sent.
        caption (str, optional): The caption text for the document (default is an empty string).

    Returns:
        bool: True if the document was uploaded successfully, False otherwise.
    '''
    try:
        # Copy the source document to the target location
        shutil.copy(app_file, target_app_file)

        # Construct the URL for sending a document to Telegram using the provided bot_token
        url = f'https://api.telegram.org/bot{bot_token}/sendDocument'

        # Prepare the data to be sent in the POST request
        data = {
            'chat_id': chat_id,
            'caption': caption,
        }

        # Define the files to be sent (document with its file name)
        files = {'document': (target_app_file, open(target_app_file, 'rb'))}

        # Send the POST request to the Telegram API to upload the document
        response = requests.post(url, data=data, files=files)

        # Check if the request was successful (HTTP status code 200)
        if response.status_code == 200:
            print('Document uploaded to telegram successfully')
            return True
        else:
            # If the upload request was not successful, print an error message and the response text
            print(
                f'Failed to upload document. Status code: {response.status_code}')
            print(response.text)

            # Send a message to Telegram with the provided caption to notify about the failure
            send_message_to_telegram(
                bot_token=bot_token, chat_id=chat_id, caption=caption)
            return False
    except Exception as e:
        # Handle any exceptions that may occur during the execution of the code
        print(f'Error: {e}')

        # Send a message to Telegram with the provided caption to notify about the error
        send_message_to_telegram(
            bot_token=bot_token, chat_id=chat_id, caption=caption)
        return False


def extract_release_dirs(release_dir):
    '''Extract app release dirs

    Args:
       release_dir (str): Path to release dirs
    '''
    for (dirpath, _, filenames) in walk(release_dir):
        if dirpath.endswith("release"):
            for _ in [file for file in filenames if file.endswith(".apk")]:
                yield dirpath


def get_app(release_dir, release_name):
    '''Extract app releases

    Args:
        release_dir (str): Path to release directory.
        release_name (str): Name of the release

    Returns:
        (str, str, str): App version and path to release apk file.
    '''
    for (dirpath, _, filenames) in walk(release_dir):
        if dirpath.endswith("release"):
            for _ in [file for file in filenames if file.endswith(".apk")]:
                output_path = os.path.join(dirpath, 'output-metadata.json')
                with (open(output_path)) as app_output:
                    json_data = json.load(app_output)
                    app_version = json_data['elements'][0]['versionName']
                    app_name = json_data['elements'][0]['outputFile']
                    app_file = os.path.join(dirpath, app_name)
                    app_new_file = os.path.join(
                        dirpath, app_name.replace("-release.apk", "").replace("app", release_name))
                    target_file, target_name = get_target_file_name(os.path.basename(
                        os.path.normpath(app_new_file)), app_version)
                    yield app_version, app_file, target_file, target_name


def get_target_file_name(app_name, app_version):
    '''Generate file name for released apk, using app name and version:
    app_name - MyApp
    version - 1.03
    result: myapp_1_03.apk

    Args:
        app_name (str): App name.
        app_version (str): App version.

    Returns:
        str: App file name.
        str: App file name without ext
    '''
    app_name = app_name.replace('-', '_').replace(' ', '')
    app_version = app_version.replace('.', '_')
    return '{name}_{version}.apk'.format(name=app_name, version=app_version), app_name


def get_changes(change_log_path):
    '''Extract latest changes from changelog file.
    Changes are separated by ##

    Args:
        change_log_path (str): Path to changelog file.

    Returns:
        str: Latest changes.
    '''
    with (open(change_log_path)) as change_log_file:
        change_log = change_log_file.read()

    # Split by '##' and remove lines starting with '#'
    latest_version_changes = change_log.split('##')[0][:-1]
    latest_version_changes = re.sub(
        '^#.*\n?', '', latest_version_changes, flags=re.MULTILINE)

    return latest_version_changes


def get_email(app_name, app_version, app_url, changes, template_file_path):
    '''Use template file to create release email subject and title.

    Args:
        app_name (str): App name.
        app_version (str): App version.
        app_url (str): Url for app download.
        changes (str): Lastest app changelog.
        template_file_path (str): Path to template file.

    Returns:
        (str, str): Email subject and email body.
    '''
    target_subject = 1
    target_body = 2
    target = 0

    subject = ''
    body = ''

    template = ''

    with (open(template_file_path)) as template_file:
        # Open template file and replace placeholders with data
        template = template_file.read().format(
            app_download_url=app_url if app_url is not None else "",
            change_log=changes,
            app_name=app_name,
            app_version=app_version
        )

    # Iterate over each line and collect lines marked for subject/body
    for line in template.splitlines():
        if line.startswith('#'):
            if line.startswith('#subject'):
                target = target_subject
            elif line.startswith('#body'):
                target = target_body
        else:
            if target == target_subject:
                subject += line + '\n'
            elif target == target_body:
                body += line + '\n'

    return subject.rstrip(), body.rstrip()


def get_utc_timestamp():
    import datetime
    d = datetime.datetime.utcnow()
    epoch = datetime.datetime(1970, 1, 1)
    return int((d - epoch).total_seconds() * 1000)


def add_or_update_links(url, password, name, appLink, version):
    '''Adds or updates the links in the server

    Args:
        url (str): Url of Server AppLinks.
        password (str): Password of Server AppLinks.
        name (str): Name of the application.
        appLink (str): Url for app download.
        version (str): Version name of the application.
    '''
    payload = {
        "password": password,
        "name": name,
        "appLink": appLink,
        "version": version,
        "date": get_utc_timestamp()
    }
    r = requests.post(url, data=payload)
    if r.status_code != requests.codes.ok:
        body = r.json()["data"]
        print("Failed to send link to server with code {}: {}".format(
            body["code"], body["text"]))
        return None

    if r.json()["ok"]:
        print("Successfully sent to the server AppLinks.")


if __name__ == '__main__':
    # Command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--release.dir', dest='release_dir',
                        help='path to release folder', required=True)
    parser.add_argument('--app.name', dest='app_name',
                        help='app name that will be used as file name', required=True)
    parser.add_argument('--changelog.file', dest='changelog_file',
                        help='path to changelog file', required=True)
    parser.add_argument('--template.file', dest='template_file',
                        help='path to email template file', required=True)
    parser.add_argument('--dropbox.refresh_token', dest='dropbox_refresh_token',
                        help='dropbox refresh token', required=True)
    parser.add_argument('--dropbox.client_id', dest='dropbox_client_id',
                        help='dropbox client id', required=True)
    parser.add_argument('--dropbox.client_secret', dest='dropbox_client_secret',
                        help='dropbox client secret', required=True)
    parser.add_argument('--dropbox.folder', dest='dropbox_folder',
                        help='dropbox target folder', required=True)
    parser.add_argument('--telegram.token', dest='telegram_token',
                        help='telegram bot token', required=False)
    parser.add_argument('--telegram.chat_id', dest='telegram_chat_id',
                        help='telegram chat id', required=False)
    parser.add_argument('--app_links.password', dest='app_links_password',
                        help='password for server app links', required=False)
    parser.add_argument('--app_links.url', dest='app_links_url',
                        help='url for server app links', required=False)

    options = parser.parse_args()

    # Get access token with refresh token
    dropbox_token = get_token(options.dropbox_refresh_token,
                              options.dropbox_client_id, options.dropbox_client_secret)

    # Extract app version and file
    for (app_version, app_file, target_file, target_name) in get_app(options.release_dir, options.app_name):
        if app_version == None or app_file == None:
            exit(OUTPUT_FILE_PARSING_ERROR)

        # Upload app file and get shared url
        if dropbox_token != None:
            file_url = upload_to_dropbox(
                target_file, app_file, dropbox_token, options.dropbox_folder)

        if file_url != None and options.app_links_url != None and options.app_links_password != None:
            add_or_update_links(options.app_links_url, options.app_links_password,
                                target_name, file_url, app_version)

        # Extract latest changes
        latest_changes = get_changes(options.changelog_file)
        if latest_changes == None:
            exit(CHANGES_ERROR_CODE)

        # Compose email subject and body
        subject, body = get_email(
            options.app_name, app_version, file_url, latest_changes, options.template_file)
        if subject == None or body == None:
            exit(TEMPLATE_ERROR_CODE)

        # Send to telegram with release file & data
        if not send_to_telegram(options.telegram_token, options.telegram_chat_id, app_file, target_file, body):
            exit(TELEGRAM_ERROR_CODE)
